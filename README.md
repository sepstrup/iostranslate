![iOS Translater.png](https://bitbucket.org/repo/xd4kX4/images/1888162553-iOS%20Translater.png)# README #

### What is this repository for? ###

Anyone who need a tool for translating iOS apps (or any app with .string translation files from x-code).

### How do I get set up? ###

Select 2 files L1 & L2 (Language1 and Language2). Pres start. Files will load and you will see the key, comment and the 2 strings for translation.
It takes .string files, default x-code format for translations.

For easy generation of .string files I use the python project igenstrings, which has proper merging (unlike Apples own genstrings). https://pypi.python.org/pypi/igenstrings

### Contribution guidelines ###

Feel free to improve it!

### Who do I talk to? ###

Owner: peter@sepstrupnet.dk