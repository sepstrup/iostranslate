//
//  ViewController.swift
//  iOSTranslater
//
//  Created by Peter Sepstrup on 6/3/17.
//  Copyright © 2017 powerLABS. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    @IBOutlet weak var browseFileL1: NSButton!
    @IBOutlet weak var file1Lbl: NSTextField!
    @IBOutlet weak var browseFileL2: NSButton!
    @IBOutlet weak var file2Lbl: NSTextField!
    @IBOutlet weak var startBtn: NSButton!
    @IBOutlet weak var commentView: NSTextField!
    @IBOutlet weak var l1Editor: NSTextField!
    @IBOutlet weak var l2Editor: NSTextField!
    @IBOutlet weak var keyView: NSTextField!
    @IBOutlet weak var prevBtn: NSButton!
    @IBOutlet weak var nextBtn: NSButton!
    @IBOutlet weak var nextUntranslatedBtn: NSButton!
    @IBOutlet weak var saveTranslationsBtn: NSButton!
    
    var file1: URL?
    var file2: URL?
    
    var translationMap: TranslationMap?
    
    var currentKey: String?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }

    @IBAction func browseFileL1(_ sender: Any) {
        openFilePickModel(sender: sender as! NSButton)
    }
    
    @IBAction func browseFileL2(_ sender: Any) {
        openFilePickModel(sender: sender as! NSButton)
    }
    
    @IBAction func startTranslation(_ sender: Any) {
        let l1 = openFileFrom(url: file1!)
        let l2 = openFileFrom(url: file2!)
        translationMap = TranslationMap(sourceLangFile: l1, destinationLanguageFile: l2)
        startTranslation()
    }
    
    @IBAction func prevBtnPressed(_ sender: Any) {
        updateCurrentTranslation()
        var lastKey = ""
        for (key, _) in (translationMap?.mappedTranslations)! {
            if key == currentKey && !lastKey.isEmpty {
                updateEditorViewFromTranslation(t: (translationMap?.mappedTranslations[lastKey])!)
                return
            }
            lastKey = key
        }
    }
    
    @IBAction func nextBtnPressed(_ sender: Any) {
        updateCurrentTranslation()
        var breakNext: Bool = false
        for (key, translation) in (translationMap?.mappedTranslations)! {
            if breakNext {
                updateEditorViewFromTranslation(t: translation)
                return
            }
            if key == currentKey {
                breakNext = true
            }
        }
    }
    
    @IBAction func nextUntranslatedBtnPressed(_ sender: Any) {
        updateCurrentTranslation()
        for (_, translation) in (translationMap?.mappedTranslations)! {
            if translation.isUntranslated() {
                updateEditorViewFromTranslation(t: translation)
                return
            }
        }
    }
    
    
    @IBAction func saveTranslationsBtnPressed(_ sender: Any) {
        updateCurrentTranslation()
        saveFileTo(url: file1!, content: (translationMap?.getL1File())!)
        saveFileTo(url: file2!, content: (translationMap?.getL2File())!)
    }
    
    func startTranslation() {
        let t = translationMap?.mappedTranslations.values.first
        updateEditorViewFromTranslation(t: t!)
        l1Editor.isEnabled = true
        l2Editor.isEnabled = true
        prevBtn.isEnabled = true
        nextBtn.isEnabled = true
        nextUntranslatedBtn.isEnabled = true
        saveTranslationsBtn.isEnabled = true
    }
    
    func updateCurrentTranslation() {
        let t = translationMap?.mappedTranslations[currentKey!]
        t?.l1 = l1Editor.stringValue
        t?.l2 = l2Editor.stringValue
    }
    
    func updateEditorViewFromTranslation(t: Translation) {
        keyView.stringValue = t.translationKey
        currentKey = t.translationKey
        commentView.stringValue = t.comment
        l1Editor.stringValue = t.l1
        l2Editor.stringValue = t.l2
    }
    
    func activateStartBtn() {
        if file1 != nil && file2 != nil {
            startBtn.isEnabled = true
        }
    }
    
    func openFilePickModel(sender: NSButton) {
        
        let dialog = NSOpenPanel();
        
        dialog.title                   = "Choose a .txt file";
        dialog.showsResizeIndicator    = true;
        dialog.showsHiddenFiles        = false;
        dialog.canChooseDirectories    = true;
        dialog.canCreateDirectories    = true;
        dialog.allowsMultipleSelection = false;
        dialog.allowedFileTypes        = ["strings"];
        
        if (dialog.runModal() == NSModalResponseOK) {
            let result = dialog.url // Pathname of the file
            
            if sender.title == browseFileL1.title {
                file1Lbl.stringValue = (result?.description)!
                file1 = result
            } else {
                file2Lbl.stringValue = (result?.description)!
                file2 = result
            }
            activateStartBtn()
        } else {
            // User clicked on "Cancel"
            return
        }

    }
    
    func openFileFrom(url: URL) -> String {
        do {
            let t = try String(contentsOf: url, encoding: String.Encoding.utf8)
            return t
        } catch {
            print("error: can't open file")
        }
        return ""
    }
    
    func saveFileTo(url: URL, content: String) {
        do {
            try content.write(to: url, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            print("can not save file")
        }
    }
    
    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

