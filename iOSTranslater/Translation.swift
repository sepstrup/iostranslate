//
//  Translation.swift
//  iOSTranslater
//
//  Created by Peter Sepstrup on 7/3/17.
//  Copyright © 2017 powerLABS. All rights reserved.
//

import Foundation

class Translation {
    
    var translationKey: String = ""
    var _l1: String = ""
    var l1: String {
        get {
            var t = _l1.replacingOccurrences(of: "\";", with: "")
            t.remove(at: t.startIndex)
            return t
        }
        set(t) {
            if l1Original.isEmpty {
                _l1 = t
                l1Original = t
            } else {
                _l1 = "\"" + t + "\";"
            }
        }
    }
    var _l2: String = ""
    var l2: String {
        get {
            var t = _l2.replacingOccurrences(of: "\";", with: "")
            t.remove(at: t.startIndex)
            return t
        }
        set(t) {
            if l2Original.isEmpty {
                _l2 = t
                l2Original = t
            } else {
                _l2 = "\"" + t + "\";"
            }
        }
    }
    var l1Original: String = ""
    var l2Original: String = ""
    var comment: String = ""
    
    func isUntranslated() -> Bool {
        var strippedKey = translationKey
        strippedKey.remove(at: translationKey.startIndex)
        let i = strippedKey.index(strippedKey.endIndex, offsetBy: -2)
        strippedKey = strippedKey.substring(to: i)
        if strippedKey == l1 || strippedKey == l2 {
            return true
        } else {
            return false
        }
    }
    
}
