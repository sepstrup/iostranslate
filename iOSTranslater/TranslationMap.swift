//
//  TranslationMap.swift
//  iOSTranslater
//
//  Created by Peter Sepstrup on 7/3/17.
//  Copyright © 2017 powerLABS. All rights reserved.
//

import Foundation

class TranslationMap {
    
    var mappedTranslations: [String:Translation] = [:]
    var l1File: String
    var l2File: String
    var l1Lines: [String] = []
    var l2Lines: [String] = []
    
    init(sourceLangFile: String, destinationLanguageFile: String) {
        l1File = sourceLangFile
        l2File = destinationLanguageFile
        l1Lines = splitToLines(textChunk: l1File)
        l2Lines = splitToLines(textChunk: l2File)
        mappedTranslations = mapKeysAndTranslation(translations: [l1Lines, l2Lines])
    }
    
    func splitToLines(textChunk: String) -> [String] {
        var fileLines: [String] = []
        textChunk.enumerateLines { (line, _) -> () in
            fileLines.append(line)
        }
        return fileLines
    }
    
    func mapKeysAndTranslation(translations: [[String]]) -> [String:Translation] {
        var mappedTranslations: [String:Translation] = [:]
        for x in 0...translations.count - 1 {
            let currentLang = translations[x]
            for i in 0...(currentLang.count - 1) {
                if currentLang[i].hasPrefix("\"") {
                    let parts = currentLang[i].components(separatedBy: " = ")
                    if x == 0 {
                        let t = Translation()
                        t.translationKey = parts[0]
                        t.l1 = parts[1]
                        t.comment = currentLang[i-1]
                        mappedTranslations.updateValue(t, forKey: t.translationKey)
                    } else {
                        mappedTranslations[parts[0]]?.l2 = parts[1]
                    }
                    
                }
            }
        }
        return mappedTranslations
    }
    
    func getL1File() -> String {
        for (_, translation) in mappedTranslations {
            if translation.l1 != translation.l1Original {
                print(translation._l1)
                l1File = l1File.replacingOccurrences(of: translation.l1Original, with: translation._l1)
            }
        }
        return l1File
    }
    
    func getL2File() -> String {
        for (_, translation) in mappedTranslations {
            if translation.l2 != translation.l2Original {
                print(translation._l2)
                l2File = l2File.replacingOccurrences(of: translation.l2Original, with: translation._l2)
            }
        }
        return l2File
    }
    
}
